# H5PreLoadDemo

_version update 2021-7-15_

## 文档修改日志

修改日期 | 修改内容
----|----
2020-04-17 | 1.添加Android 专案错误排除
2021-07-15 | 1.添加MIME-TYPE 修正加载错误

## H5预加载实现示例 

本示例适用版本为cocos creator 2.1.2

手机 H5 预加载是指在手机 APP 中，将手机 H5 版本的资源下载到玩家手机后运行的一 种方式。这种方式跟手机H5版本嵌入APP类似，只是后者每次都会从网络验证和请求资源， 前者只需在版本更新后下载一次。这样做的好处主要是提高资源加载速度，并解决因网络等 原因导致的资源加载慢、无法加载问题。

本示例为实现分包下载，根据bgPreload.json，下载资源描述文件。然后 将之与本地上次更新的 bgPreload.json(每次资源成功更新后需要将 bgPreload.json 保存到本地以便检测资源更新)比较，如果发现版本有差异，则需要按照 bgPreload.json 的规范更新。

### 示例分支master

示例js下载bgPreload.json ，根据条件判断是否需要下载或更新本地资源

### 示例分支android-

示例android所使用NanoHTTPD 本地web service，zip解压缩与md5计算。
android 9 http处理。

### 示例分支ios-

示例ios所使用CocoaHttpServer 本地web service，zip解压缩与md5计算以及WKWebView开启示范内容。

ios本地h5开启建议使用WKWebView开启。

### 啟動參數

网址参数请参考API获取，参数请参考对接文档。

### 啟動參數

网址参数请参考API获取，参数请参考对接文档。

### 问题排除

#### Android 专案

##### 导入专案 出现 Gradle 'proj.android-studio' project refresh failed  Error:A problem occurred configuring project ':hello_world'.的错误。

确认Project Structure -> Android NDK location 是否有下载并设置，推荐android-ndk-r16b以上版本

#### IOS 专案

##### 程序 crash  HTTPConnection::filePathForURI 函数中 config 的 documentroot 是空的

选择项目 target，Build Phases –> Compile Sources 中，给目錄 Core 、CocoaLumberjack、 CocoaAsyncSocket 下的代码添加 Compiler Flags ： -fobjc-arc