var tools = require("tools");

cc.Class({
    extends: cc.Component,

    properties: {
        isPortrait:false,
        webView:cc.Node,
        editBox:cc.EditBox,
        msg:require("msgbar"),
        UpdateBtn:cc.Button,
        UpdateLabel:cc.Label,
    },
    // use this for initialization
    onLoad: function () {
        //本机Server 资料夹
        if (cc.sys.OS_IOS == cc.sys.os || cc.sys.OS_ANDROID == cc.sys.os){
            this.WebRoot = jsb.fileUtils.getWritablePath() + "webRoot/";
            if( !jsb.fileUtils.isDirectoryExist( this.WebRoot ) ){
                jsb.fileUtils.createDirectory( this.WebRoot );
            }
            if (cc.sys.OS_IOS == cc.sys.os) {
                //ios 开启本机http
                 console.log("开启本机http = "+jsb.fileUtils.getWritablePath() + "webRoot/");
                jsb.reflection.callStaticMethod("AppController", "ServerStart:",jsb.fileUtils.getWritablePath() + "webRoot/");
            }
        }
        //下载范例资料夹 
        this.GameFolder = "Sample";
        //解压缩完成callback
        window.UnZipReturn = function(b){
            console.log("解压缩:"+b);
            console.log("下一个");
            this.CalunZip();
        }.bind(this);
        window.getMessage = tools.getMessage;
        window.webView = this.webView;

       // var a=jsb.fileUtils.isFileExist(this.WebRoot + this.GameFolder+"/"+"bgPreload.json");
       // console.log("aaa=",a);
       // this.test(1)
    },
    start:function(){
         if (cc.sys.OS_IOS == cc.sys.os || cc.sys.OS_ANDROID == cc.sys.os){
             if(jsb.fileUtils.isFileExist(this.WebRoot + this.GameFolder+"/"+"bgPreload.json")){
                 this.setStartButton( true , "开启WebView" );
             }else{
                 this.setStartButton( false , "等待更新" );
             }
         }
    },
    setStartButton:function( intState_ , str_ ){
         this.UpdateBtn.interactable = intState_;
         this.UpdateLabel.string = str_;
    },
    //cui 64   锁定横板+256
    opWebView:function(){

        var Url_parameter = "";
        var editBoxStr = "";
        var editBoxStrTmp = null;
        //
        if(this.editBox && this.editBox.string != ""){
            editBoxStrTmp = this.editBox.string.split("?");
            if(editBoxStrTmp.length == 2){
                Url_parameter ="?"+editBoxStrTmp[1];
            }else{
                //Url_parameter ="?"+editBoxStrTmp[0];
                if(Url_parameter.indexOf('?') == -1){
                    Url_parameter ="?"+editBoxStrTmp[0];
                }
            }
            var tmp_ = tools.getMessage(Url_parameter,"return=");
            if(tmp_ ==""){
                Url_parameter += "return=http://www.test.com"
            }

        }else{
            var cui_ = 64;
            if(this.isPortrait){
                cui_+=128;
            }else{
                cui_+=256;
            }
            Url_parameter = "?cui="+cui_+"&return=http://www.test.com";
        }
       
        //网址参数请参考API获取
        //提示cui return参数参考对接文档
        console.log("Url_parameter =",Url_parameter); 
        if (cc.sys.OS_IOS == cc.sys.os || cc.sys.OS_ANDROID == cc.sys.os){
            if(!jsb.fileUtils.isFileExist(this.WebRoot + this.GameFolder+"/"+"bgPreload.json")){
                console.log("游戏档案不存在，请下载更新。" );
                this.msg.addmsg("游戏档案不存在，请下载更新。");
               return;
            }
        }
        if (cc.sys.OS_IOS == cc.sys.os){
            this.URL = "http://localhost:8888/"+this.GameFolder+"/index.html" + Url_parameter ;
            var URL_ = this.URL.split("?")
            var a = "return=";
            var b = tools.getMessage(URL_[1],a);
            var tmp = "";
            var scheme = "";
            if(b && b!=""){
                tmp = b.split("://");
                scheme = tmp[0].replace("=","");
            }
            jsb.reflection.callStaticMethod("RootViewController", "openWebView:Scheme:ReturnUrl:", this.URL , scheme , tmp[1]);
        }else{
            if(cc.sys.OS_ANDROID == cc.sys.os){
                this.URL = "http://localhost:8888/"+this.GameFolder+"/index.html" + Url_parameter ;
                var URL_ = this.URL.split("?")
                var a = "return=";
                var b = tools.getMessage(URL_[1],a);
                var tmp = "";
                var scheme = "";
                if(b && b!=""){
                    tmp = b.split("://");
                    scheme = tmp[0].replace("=","");
                    if(b[b.length -1]!= "/"){
                        b=b+"/";
                    }
                }
                jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity", "AddWebView", "(Ljava/lang/String;Ljava/lang/String;)V",this.URL , b);
            }else{
                if(this.webView.active){
                    this.webView.active = false;
                }else{
                    this.webView.active = true;
                    let a = this.webView.getComponent(cc.WebView);
                    a.url = this.URL;
                }
            }
            // this.URL = "http://localhost:8888/"+this.GameFolder+"/index.html" + Url_parameter ;

        }
        console.log("开启网址 = ",this.URL );
    },
    downloadJson:function(){
        //场景按钮第一个呼叫function
        this.msg.clear();
        let url_ = "https://pl.vdcache.run/bg/bgVideo/bgPreload.json";
        tools.getFile( {url: url_ , type:0} , this.getRemoteJsonCallBack.bind(this) );
        console.log("this.WebRoot = ",this.WebRoot)
        
        if (cc.sys.OS_IOS == cc.sys.os || cc.sys.OS_ANDROID == cc.sys.os){
            //var dirpath =  this.WebRoot;// jsb.fileUtils.getWritablePath() + 'root/';
            //检查是否要建立sample资料夹
            let b = jsb.fileUtils.isDirectoryExist( this.WebRoot + this.GameFolder )
            console.log(  this.WebRoot + this.GameFolder +" 是否存在? ", b);
            if( !jsb.fileUtils.isDirectoryExist( this.WebRoot + this.GameFolder ) ){
                jsb.fileUtils.createDirectory( this.WebRoot + this.GameFolder );
                console.log("建立", this.WebRoot + this.GameFolder);
            }
        }
        this.setStartButton( false , "等待更新" );
    },
    downloadJson2:function(){
        //测试不同版本preLoad更新用 网址请自行更换与建立
        //场景按钮第一个呼叫function
        this.msg.clear();
        let url_ = "http://10.37.1.30:8000/sample/bgPreload.json";
        tools.getFile( {url: url_ , type:0} , this.getRemoteJsonCallBack.bind(this) );
        console.log("this.WebRoot = ",this.WebRoot)
        if (cc.sys.OS_IOS == cc.sys.os || cc.sys.OS_ANDROID == cc.sys.os){
            //var dirpath =  this.WebRoot;// jsb.fileUtils.getWritablePath() + 'root/';
            //检查是否要建立sample资料夹
            let b = jsb.fileUtils.isDirectoryExist( this.WebRoot + this.GameFolder )
            console.log(  this.WebRoot + this.GameFolder +" 是否存在? ", b);
            if( !jsb.fileUtils.isDirectoryExist( this.WebRoot + this.GameFolder ) ){
                jsb.fileUtils.createDirectory( this.WebRoot + this.GameFolder );
                console.log("建立", this.WebRoot + this.GameFolder);
            }
        }
        this.setStartButton( false , "等待更新" );
    },
    //      callback(null , Req.response , type , url , index);
    getRemoteJsonCallBack:function( err , t ){
        if(err){
            console.log("取得远端bgPreload 失败 ,err=",err);
             this.msg.addmsg("取得远端bgPreload 失败");
        }else{
            console.log("取得远端bgPreload");
            this.msg.addmsg("取得远端bgPreload");
            this.Remote_Preload = tools.strFormate(t);
            this.Remote_PreloadObj = JSON.parse( this.Remote_Preload );
            this.Preload = null;
            //检查bgPreload.json是否存在 若存在 读取比对版本看是否需要更新
            if (cc.sys.OS_IOS == cc.sys.os || cc.sys.OS_ANDROID == cc.sys.os){
                if(jsb.fileUtils.isFileExist(this.WebRoot + this.GameFolder+"/"+"bgPreload.json")){
                    //存在 载入bgPreload
                    console.log("取得本机bgPreload.json path_=",this.WebRoot + this.GameFolder+"/"+"bgPreload.json");
                    let b = jsb.fileUtils.getStringFromFile(this.WebRoot + this.GameFolder+"/"+"bgPreload.json");
                    
                    
                    console.log(JSON.stringify(b));
                    this.getJsonCallBack(b);
                    this.msg.addmsg("取得本机bgPreload.json");
                    //this.getJson( {url:this.WebRoot + this.GameFolder+"/"+"bgPreload.json"} , this.getJsonCallBack.bind(this) );
                }else{
                    //不存在 直接进入下载流程
                    console.log("没有本机bgPreload.json 下载更新");
                    this.msg.addmsg("没有本机bgPreload.json 下载更新");
                    this.DownLoad();
                }
            }
        }
    },
    getJsonCallBack:function(t , err ){
        if(!t || err){
            this.msg.addmsg("取得本机bgPreload失败,",err);
        }else{
            //手机里的bgPreload.json
            this.Preload = tools.strFormate(t);
            this.PreloadObj = JSON.parse( this.Preload );
            //本机版本 与线上不合  触发更新
            this.msg.addmsg("版本比对 本机"+this.PreloadObj.version+ "远端版本"+ this.Remote_PreloadObj.version);
            console.log("版本比对",this.PreloadObj.version,this.Remote_PreloadObj.version);
            if(  this.PreloadObj.version !=  this.Remote_PreloadObj.version ){
                console.log("版本不同 触发下载更新");
                this.msg.addmsg("版本不同 触发下载更新");
                this.DownLoad();
            }else{
                console.log("版本是最新版");
                this.msg.addmsg("版本是最新版");
                this.setStartButton( true , "开启WebView" );
            }
        }

    },
    //根据Json内容下载档案
    DownLoad:function(){
        let bPass = false;
        this.totalDownLoadNum = 0;
        this.NeedDownLoadNum=0;
        let tmp =  JSON.stringify(this.Remote_PreloadObj.m2.res);
        this.SourceList = JSON.parse( tmp ) ;
        for(let i =0; i <  this.Remote_PreloadObj.m2.res.length ; i++){
            //检查旧有this.Preload  是否有相同资源 有的话检查checksum是否一至
            if(this.PreloadObj){
                for(let j = 0 ; j < this.PreloadObj.m2.res.length ; j++){
                    if(this.Remote_PreloadObj.m2.res[i].path == this.PreloadObj.m2.res[j].path ){
                        if(this.Remote_PreloadObj.m2.res[i].md5 == this.PreloadObj.m2.res[j].md5){
                            bPass = true;
                        }
                        continue;
                    }
                }
            }
            if(bPass){
                continue;
            }
            this.NeedDownLoadNum++;
            console.log( "开启下载" , this.Remote_PreloadObj.url +  this.Remote_PreloadObj.m2.res[i].path , " index = ",i);
            this.msg.addmsg("下载" + this.Remote_PreloadObj.url +  this.Remote_PreloadObj.m2.res[i].path);
            tools.getFile( {url:this.Remote_PreloadObj.url +  this.Remote_PreloadObj.m2.res[i].path ,type:this.Remote_PreloadObj.m2.res[i].type , index:i} , function(err , zipBuffer ,type ,url , index){
                if(err){
                    console.log("下载 err ",err);
                }else{
                    let a = url.replace(this.Remote_PreloadObj.url ,"");//this.Remote_PreloadObj.m2.res[i].path;
                    console.log("下载 完成 ", url , a);
                    let b = a.split("/");
                    let c = null;
                    if(b.length > 1){
                        c = this.WebRoot + this.GameFolder+"/"+b[0];
                    }
                    
                    console.log(  c +" 是否存在? ");
                    if( c && !jsb.fileUtils.isDirectoryExist( c ) ){
                        jsb.fileUtils.createDirectory( c );
                        console.log("建立资料夹", c);
                    }
                    if (cc.sys.OS_IOS == cc.sys.os || cc.sys.OS_ANDROID == cc.sys.os){
                        console.log("存档",  this.GameFolder+"/"+ a);
                        if(type){
                            jsb.fileUtils.writeDataToFile(  zipBuffer  , this.WebRoot + this.GameFolder+"/"+ a/*this.Remote_PreloadObj.m2.res[i].path*/ );
                        }else{
                            jsb.fileUtils.writeStringToFile(  zipBuffer  , this.WebRoot  + this.GameFolder+"/"+ a );
                        }
                        
                        console.log("index=" ,index)
                        if(index!= null){
                            
                            if(tools.calMD5( this.WebRoot + this.GameFolder+"/"+ a , this.SourceList[index].md5 )){
                                console.log("MD5相同 下载成功");
                                this.SourceList[index].result = 1;
                                this.msg.addmsg("MD5相同 "+a+"下载成功");
                            }else{
                                console.log("MD5不相同 下载失败");
                                this.msg.addmsg("MD5不相同 "+a+"下载失败");
                                this.SourceList[index].result = 0;
                            }
                        }

                    }
                    this.totalDownLoadNum++;
                    this.checkDownLoad();
                }

            }.bind(this));
        }
    },
    checkDownLoad:function(){
        let checkIndex_ = 0;
        //if(this.totalDownLoadNum == this.Remote_PreloadObj.m2.res.length){
        if(  this.totalDownLoadNum == this.NeedDownLoadNum  ){
            console.log("全部下载完毕");
            this.msg.addmsg("全部下载完毕");
            this.NeedUnZipNum = 0;
            this.unZipIndex = 0;
            checkIndex_ = 0;
            for(let i = 0 ; i < this.SourceList.length ; i++){
                console.log("档案:",this.SourceList[i].path ,"下载结果:" ,this.SourceList[i].result);
                if(this.SourceList[i].result){
                    checkIndex_++;
                    if(this.SourceList[i].type == 1 && this.SourceList[i].result){
                        this.NeedUnZipNum++;
                    }
                }
            }
            //if(checkIndex_ == this.SourceList.length){
                if(checkIndex_ == this.totalDownLoadNum ){
                console.log("MD5检查完毕 下载成功");
                this.msg.addmsg("储存bgPreload.json");
                //转存preload.json
                if (cc.sys.OS_IOS == cc.sys.os || cc.sys.OS_ANDROID == cc.sys.os){
                    let Path_ = this.WebRoot + this.GameFolder+"/"+"bgPreload.json";
                    let bOldFile = jsb.fileUtils.isFileExist(Path_);
                    console.log( Path_ +" 是否存在? ", bOldFile);
                    if(bOldFile){
                        //移除旧档案
                         jsb.fileUtils.removeFile(Path_);    
                    }
                    //建立新的bgPreload.json
                    jsb.fileUtils.writeStringToFile(  JSON.stringify(this.Remote_PreloadObj)  , Path_ );
                }
                this.CalunZip();
            }
        }
    },
    CalunZip:function(){
        let fileName ="";
        let outPutPath_  ="";
        let index = this.unZipIndex ;
        let bNext = false;
        if(this.unZipIndex >= this.NeedUnZipNum){
            //解压缩完毕
            console.log("解压缩目标完毕");
            this.scheduleOnce(  function(){ 
                 this.msg.addmsg("解压缩完毕 可以开启H5视讯"); 
                 this.setStartButton( true , "开启WebView" );
            }.bind(this) , 0.2   );
        }else{
            this.unZipIndex++;
            console.log("unzipStatus",this.SourceList[index].type ,this.SourceList[index].path , this.SourceList[index].result  );
            if(this.SourceList[index].type && this.SourceList[index].result){
                if (cc.sys.OS_IOS == cc.sys.os) {
                    fileName = this.WebRoot  + this.GameFolder+"/"+this.SourceList[index].path;
                    var tmp_ = this.SourceList[index].path.split("/");
                    var tmpPath_ = "";
                    if(tmp_.length > 1){
                        for(let i = 0 ; i < tmp_.length - 1 ; i++){
                            tmpPath_ += tmp_[i];
                        }
                    }
                    outPutPath_ = this.WebRoot  + this.GameFolder+"/"+tmpPath_;
                    console.log("解压缩目标 = ",fileName , index , this.unZipIndex,fileName,outPutPath_);
                    jsb.reflection.callStaticMethod("AppController", "unZip:OutPutPath:",fileName,outPutPath_);
                }
                else if (cc.sys.OS_ANDROID == cc.sys.os) {
                    fileName = this.WebRoot  + this.GameFolder+"/"+this.SourceList[index].path;
                    console.log("解压缩目标 = ",fileName , index , this.unZipIndex);
                    jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity", "unZip", "(Ljava/lang/String;)V", fileName);

                }
                this.msg.addmsg("解压缩"+fileName);
            }else{
                console.log("不需要解压缩 下一个");
                this.CalunZip();
            }
            
        }
    },

});
