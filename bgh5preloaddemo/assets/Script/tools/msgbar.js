
cc.Class({
    extends: cc.Component,

    properties: {
        sc:cc.ScrollView,
        content:cc.Node,
        item:cc.Node,
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

    },
    clear:function(){
        this.content.removeAllChildren();
    },
    addmsg:function(msg_){
        let a = cc.instantiate(this.item);
        
        this.content.addChild(a);
        a.active = true;
        a.getComponent(cc.Label).string = msg_ + "";

        this.sc.scrollToBottom(0.1);
    },
    // update (dt) {},
});
