var tools = cc.Class({
    statics: {
        instance: null,
    },
    getREADME(){
        return "AAA";
    },
    getMessage: function (OrginMsg, tar, sliceSambol = "&") {
        let tarStr = "";
        let tmpIndex = 0;
        let whileIdx = 0;
        let str_ = "";
        //取得locale 
        tarStr = tar;
        tmpIndex = OrginMsg.search(tarStr);
        if(tmpIndex == -1){
            return str_;
        }
        whileIdx = tmpIndex;
        whileIdx += tarStr.length;
        str_ = "";

        while (whileIdx < OrginMsg.length) {
            if (OrginMsg[whileIdx] == "&") {
                break;
            } else {
                str_ = str_ + OrginMsg[whileIdx];
            }

            whileIdx++;
        }
        return str_;
    },
    getFile:function(item , callback){
        //type = 1 为zip
        let url = item.url;
        let index = item.index;
        let type = item.type
        let returnData = null;
        var Req = new XMLHttpRequest(); 
        // 定义连线方式
        Req.open('GET', url);
        if(type){
            //type = 1 为 zip档 
            Req.responseType = 'arraybuffer';
        }
        // 送出请求
        Req.send();
        Req.timeout = 300000;
        // 如果成功就执行 reqOnload()
        Req.onload = function(){
        }; 
        Req.onreadystatechange = function () {
            cc.log("xhr.readyState  " +Req.readyState);
            cc.log("xhr.status  " +Req.status);
            if (Req.readyState === 4 ) {
                if(Req.status === 200){
                    //responseType一定要在外面设置
                   // console.log('Req.response', Req.response);
                   if(type){
                       //zip
                        var arrayBuffer = Req.response;
                        var result = new Uint8Array(arrayBuffer);
                        console.log("getZip return = ",url)
                        //callback(null, result,arrayBuffer , url , checkMd5);
                        callback(null, result,type , url , index);
                   }else{
                       //no zip
                        console.log("getJson return = ",url)
                        callback(null , Req.response , type , url , index);
                   }

                }else{
                    callback("Error");
                    console.log('错误', err);
                }
            }
        }.bind(this);
        // 失败就 reqError()
        Req.onerror = function(err){
            callback("Error"); // 第一个参数需要传递错误信息
            console.log('错误', err);
        };
        Req.ontimeout  = function(err){
            callback("超时"); // 第一个参数需要传递错误信息
            console.log('超时', err);
        };
    },
        //去掉所有的换行符
    //去掉所有的空格（中文空格、英文空格都会被替换）
    strFormate:function(s){
        let a = s;
        a = a.replace(/\r\n/g,"");
        a = a.replace(/\n/g,"");
        a = a.replace(/\s/g,"");
        return a;
    },
    calMD5:function( fileName , checkMD5){
         let fileMd5 = "";
         let check_ = false;
            console.log("checkMD5 = " , checkMD5);
            console.log("fileName = " , fileName);
        if (cc.sys.OS_IOS == cc.sys.os) {
            fileMd5 = jsb.reflection.callStaticMethod("AppController", "file_md5:",fileName);
        }
        else if (cc.sys.OS_ANDROID == cc.sys.os) {
            fileMd5 = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity", "calMD5", "(Ljava/lang/String;)Ljava/lang/String;", fileName);
        }
        console.log("fileMd5=",fileMd5 ,"checkMD5 = " , checkMD5);
        if(fileMd5 == checkMD5){
            check_ = true;
        }
        return check_;
    },
});
tools.instance = new tools();
module.exports = tools.instance;